import 'package:flutter/material.dart';
import 'package:lab16_peliculas/src/widgets/card_swiper_widget.dart';
import 'package:lab16_peliculas/src/providers/peliculas_provider.dart';
import 'package:lab16_peliculas/src/widgets/carousel_slider_widget.dart';
import 'package:lab16_peliculas/src/widgets/horizontal_list.dart';

class HomePage extends StatelessWidget {

  final peliculasProvider = new PeliculasProvider();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Peliculas en Cines'),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.search), onPressed: null,)
        ],
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            _swipperTarjetas(),
            
            _sliderTarjetas(),
            //_sliderHorizontal(),
          ],
        ),
      ),
    );
  }

  Widget _swipperTarjetas(){
    return FutureBuilder(
      future: peliculasProvider.getEnCines(),
      builder: (BuildContext context, AsyncSnapshot<List> snapshot){
        if(snapshot.hasData){
          return CardSwiper(peliculas: snapshot.data);
        }else{
          return Container(
            height: 400.0,
            child: Center(child: Text('no hay')),
            //child: Center(child: CircularProgressIndicator(),),
          );
        }
      },

    );
  }

  Widget _sliderTarjetas(){
    return FutureBuilder(
      future: peliculasProvider.getEnCines(),
      builder: (BuildContext context, AsyncSnapshot<List> snapshot){
        if(snapshot.hasData){
          return ImagenSlider(peliculas: snapshot.data);
        }else{
          return Container(
            height: 400.0,
            child: Center(child: Text('no hay')),
            //child: Center(child: CircularProgressIndicator(),),
          );
        }
      },

    );
  }

  Widget _sliderHorizontal(){
    return FutureBuilder(
      future: peliculasProvider.getEnCines(),
      builder: (BuildContext context, AsyncSnapshot<List> snapshot){
        if(snapshot.hasData){
          return HorizontalImage(peliculas: snapshot.data);
        }else{
          return Container(
            height: 400.0,
            child: Center(child: Text('no hay')),
            //child: Center(child: CircularProgressIndicator(),),
          );
        }
      },

    );
  }



}
