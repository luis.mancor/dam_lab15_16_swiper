
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:lab16_peliculas/src/models/pelicula_model.dart';

class PeliculasProvider {
  String _apikey = '87990749124855f8dbc3defec99f02ce';
  String _url = 'api.themoviedb.org';
  String _language = 'es-ES';
  Future <List<Pelicula>> getEnCines() async {
  final url = Uri.https(_url, '3/movie/now_playing', {
      'api_key': _apikey,
      'language': _language
    });
  final resp = await http.get(url);
  final decodedData = json.decode(resp.body);
  final peliculas = new Peliculas.fromJsonList(decodedData['results']);
  //print(decodedData['results']);
  //print(peliculas.items[0].title);
  //return [];
    return peliculas.items;
  }
}


/*
void main(List<String> arguments) async {
  // This example uses the Google Books API to search for books about http.
  // https://developers.google.com/books/docs/overview
  var url = 'https://www.googleapis.com/books/v1/volumes?q={http}';

  // Await the http get response, then decode the json-formatted response.
  var response = await http.get(url);
  if (response.statusCode == 200) {
    var jsonResponse = convert.jsonDecode(response.body);
    var itemCount = jsonResponse['totalItems'];
    print('Number of books about http: $itemCount.');
  } else {
    print('Request failed with status: ${response.statusCode}.');
  }
}
*/